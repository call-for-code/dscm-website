import { useState, useEffect } from 'react'
import ReactMapGL, { Source, GeolocateControl, NavigationControl, Layer, Popup } from 'react-map-gl'

import fetch from 'isomorphic-unfetch'
import BaseHelper from '../helpers/BaseHelper';
// import { PointerCovid } from './PointerCovid'

const LABEL_BG = ["bg-warning", "bg-primary", "bg-info", "bg-danger", "bg-success"]
const getRandomBG = () => LABEL_BG[Math.floor(Math.random() * LABEL_BG.length)]

const geolocateStyle = {
    float: 'left',
    margin: '50px',
    padding: '10px'
};


// Bentuk Tracking
const covidTrackingStyle = {
    id: 'point-bahaya',
    // onClick : {handlePointer},
    type: 'circle',
    paint: {
        "circle-opacity": 0.75,
        "circle-stroke-width": 1,
        "circle-radius": 6,
        "circle-color": "crimson"
        // "circle-radius": [
        //     "interpolate",
        //     ["linear"],
        //     ["get", "cases"],
        //     1, 4,
        //     1000, 8,
        //     4000, 10,
        //     8000, 14,
        //     12000, 18,
        //     100000, 40
        // ],
        // "circle-color": [
        //     "interpolate",
        //     ["linear"],
        //     ["get", "cases"],
        //     1, '#ffffb2',
        //     5000, '#fed976',
        //     10000, '#feb24c',
        //     25000, '#fd8d3c',
        //     50000, '#fc4e2a',
        //     75000, '#e31a1c',
        //     100000, '#b10026'
        // ],
        // "circle-stroke-width": [
        //     "interpolate",
        //     ["linear"],
        //     ["get", "cases"],
        //     1, 1,
        //     100000, 1.75,
        // ],
    }
}


const Map = () => {



    const [viewport, setViewPort] = useState({
        width: "100%",
        height: 480,
        latitude: -7.466577942406782,
        longitude: 112.7009543944117,
        zoom: 10
    })

    const [timelineCategories, setTimelineCategories] = useState([])
    const [timelines, setTimelines] = useState([])

    const [popup, setPopup] = useState({
        state: false,
        lat: 0,
        long: 0,
        data: {
            id: 0
        }
    })

    const closePopup = () => {
        setPopup({
            state: false
        })
    }


    useEffect(() => {
        const token = localStorage.getItem('token')

        async function fetchData(token) {
            BaseHelper.createFromUrlWithApiUrl("timeline_categories").get((result) => {
                if (result.status) {
                    setTimelineCategories(result.data.map(tc => {
                        tc.checked = true;
                        return tc;
                    }));
                    setTimelines(result.data.map(tc => tc.timelines).flat())


                    // setGeo(getGeo(result.data.map(tc => tc.timelines).flat()))
                } else {
                    alert('failed to get timeline');
                }
            });

        }

        fetchData(token)

    }, [])

    const handleTimelineCategoryCheckBox = (event, selectedTc) => {
        const tcs = timelineCategories;
        tcs.map(tc => {
            if (tc.id_timeline_category == selectedTc.id_timeline_category) {
                tc.checked = !tc.checked
            }
        });
        setTimelineCategories(tcs);
        setTimelines(tcs.filter(tc => tc.checked).map(tc => tc.timelines).flat(1));
    }

    const getGeo = () => {
        return {
            "type": "FeatureCollection",
            "features": timelines.filter(t => typeof t.place_lat_lon != 'undefined' && t.place_lat_lon != null).map(t => {
                let latLon = t.place_lat_lon.split(",");

                return {
                    "type": "Feature",
                    "properties": t,
                    "geometry": { "type": "Point", "coordinates": [latLon[1], latLon[0]] }
                }
            })
        }
    }

    const handlePopup = (lat, long, props) => {
        setPopup({
            state: true,
            lat: lat,
            long: long,
            data: {
                id: props.id_user,
                created_at: props.created_at + " UTC",
                username: props.username,
                labels: props.category_label.split("/"),
                description: props.description,
                category: props.category_relevant_text
            }
        })
    }

    const _onClick = (event) => {
        const feature = event.features.find(f => f.layer.id === 'point-bahaya');

        if (feature) {
            console.log(event)
            const lat = event.lngLat[1]
            const long = event.lngLat[0]
            const props = event.features[0].properties

            console.log(lat)
            handlePopup(lat, long, props)
            // alert(
            //     'ID User :' + event.features[0].properties.id_user + '\n'
            // )
        }
    }

    // const _onHover = (event) =>{
    //     const feature = event.feature.find(f => f.layer.id === 'point-bahaya')

    //     if(feature){
    //         console.log("hover")
    //     }
    // }


    // const toggleHover = () =>{
    //     setHover(!hover)
    // }

    // Untuk ganti viewport
    const _onViewportChange = viewport => setViewPort({ ...viewport, transitionDuration: 3000 })

    return (
        <div style={{ margin: '0 auto' }}>
            <h1 className="mt-2 mb-3" style={{ textAlign: 'center', fontSize: '25px', fontWeight: 'bolder' }}>Climeet Maps View </h1>



            {/* <span className="mdi mdi-presentation"></span> */}
            {/* <span>Coordinate : [ Lat : {viewport.latitude} ] [ Lang : {viewport.longitude}]</span> */}
            <ReactMapGL
                {...viewport}
                mapboxApiAccessToken="pk.eyJ1IjoibWF4eG90byIsImEiOiJjazlhM28xZ24xejB5M2VwZzRsbDdjZTg3In0.JKDRRj8PuuftG74j2WjJdw"
                mapStyle='mapbox://styles/mapbox/streets-v11'
                onViewportChange={_onViewportChange}
                transitionDuration="0"
                onClick={_onClick}

            >
                <div style={{ position: 'absolute', right: 0 }}>
                    <NavigationControl />
                </div>
                <GeolocateControl
                    style={geolocateStyle}
                    positionOptions={{ enableHighAccuracy: true }}
                    trackUserLocation={true}
                />

                {/* Kalau langsung konek API langsung ganti di data */}
                <Source id="covid19-data" type="geojson" data={getGeo()}>
                    {/* <Layer {...covidTrackingStyle}/> */}


                    {popup.state != false ? (
                        <Popup
                            latitude={popup.lat}
                            longitude={popup.long}
                            onClose={closePopup}
                            anchor='top'
                        >
                            <p> Username : {popup.data.username}<br/>
                            Posted At : {popup.data.created_at}<br/>
                            Category : {popup.data.category}<br/>
                            Tags : {popup.data.labels.map(label => <div style={{display:"inline-block",marginRight:5,marginBottom:5}} className={getRandomBG() + " text-white"}>{label}</div>)} </p>
                            <p><b>{popup.data.description}</b></p>
                        </Popup>
                    ) : <></>}

                    <Layer
                        style={{"cursor":"pointer"}}
                        id='point-bahaya'
                        type='circle'
                        paint={{
                            "circle-opacity": 0.75,
                            "circle-stroke-width": 1,
                            "circle-radius": 8,
                            "circle-color": "crimson"
                            
                        }}
                    // 6 radius
                    // onMouseEnter={toggleHover}
                    // onMouseLeave={toggleHover}
                    >



                    </Layer>

                    {/* Ternary popup */}
                    {/* {pointerClick !== null ? (
                        <Popup
                        latitude={popup.lat}
                        longitude={popup.long}
                        onClose={closePopup}
                    >
                        <p>HotSpot Information</p>
                    </Popup>
                    ) : <div></div>} */}
                    {/* <Popup
                        latitude={popup.lat}
                        longitude={popup.long}
                        onClose={closePopup}
                    >
                        <p>HotSpot Information</p>
                    </Popup> */}

                </Source>


            </ReactMapGL>

            <div className="row">
                <div className="col-md-12">
                    <hr />
                    <h4>Timeline Category</h4>
                </div>
                <div className="col-md-12">
                    <div className="form-group">
                        {timelineCategories.map(tc => <div style={{ display: 'inline-block', marginRight: 10 }} className="form-check form-check-primary">
                            <label className="form-check-label">
                                <input type="checkbox" defaultChecked={tc.checked} onChange={(e) => handleTimelineCategoryCheckBox(e, tc)} className="form-check-input" /> {tc.name} <i className="input-helper" /></label>
                        </div>)}
                    </div>
                </div>
            </div>
        </div >
    )
}

export default Map