

const LABEL_BG = ["bg-warning", "bg-primary", "bg-info", "bg-danger", "bg-success"]

const getRandomBG = () => LABEL_BG[Math.floor(Math.random() * LABEL_BG.length)]

const TimelineCard = (props) => (
    <div className="col-lg-6" style={{marginBottom: 30}}>
        <div className="card">
            <div className="card-body row">
                <div className="col-lg-8">
                    {props.mediaUrl != null ? <img style={{ width: '100%', marginBottom: 20 }} src={props.mediaUrl} /> : null}
                    <h4>{props.description}</h4>
                </div>
                <div className="col-lg-4">
                    <h4>{props.username}</h4>
                    <h6>Posted at : {props.createdAt}</h6>
                    <h6>Category : {props.category}</h6>
                    <h6>Tags : {props.labels.map(label => <div style={{marginRight:5,marginBottom:5}} className={getRandomBG() + " text-white"}>{label}</div>)}</h6>
                </div>
            </div>
        </div>
    </div>
)

export default TimelineCard;