

const TimelineGroupCard = (props) => (
    <div className="col-lg-3 grid-margin">
        <div className="card">
            <div className="card-body">
                <center>
                    <h3>{props.name}</h3>
                    <h1>{props.count}</h1>
                    <button type="button" onClick={props.onClick} style={{ marginTop: '30px' }} className="btn btn-gradient-primary btn-lg btn-block">
                        <i className="mdi mdi-eye"></i> Show timelines </button>
                </center>
            </div>
        </div>
    </div>
)

export default TimelineGroupCard;