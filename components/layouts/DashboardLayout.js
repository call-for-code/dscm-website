

import React, { useContext } from 'react'

// import { UserContext } from '../../pages/helpers/authContext'

// KOMPONEN
import Navbar from './Navbar'
import Sidebar from './Sidebar'

class DashboardLayout extends React.Component {

  // const { signOut } = useContext(UserContext)
  state = {
    render:false
  }

  componentDidMount(){
    if(localStorage.getItem("token") == null){
      window.location.href = "/"
    }else{
      this.setState({render:true  })
    }
  }

  render() {
    return this.state.render?(
      <div>
        <div className="container-scroller">
          <Navbar signOut={this.props.signOut} />
          <div className="container-fluid page-body-wrapper">
            <Sidebar />
            <div className="main-panel">
              {this.props.children}
            </div>
          </div>
        </div>
      </div >
    ):null;
  }

}

export default DashboardLayout