import { useState } from 'react'

import Router from 'next/router'

const Navbar = (props) => {
    const [isCollapse, setCollapse] = useState(false)    
    const [isProfileShop,setProfShow] = useState(false)
    const [isNotif, setNotif] = useState(false)
    
    // const { signOut } = useContext(UserContext)

    const handleCollapseMobile = () => {
        const sidebar = document.getElementById('sidebar')
        if (isCollapse == false) {
            setCollapse(true)
            sidebar.classList.add('active')

        } else {
            setCollapse(false)
            sidebar.classList.remove('active')
        }
    }

    const handleCollapse = () => {
        if (isCollapse == false) {
            setCollapse(true)
            document.body.classList.add('sidebar-icon-only')
        } else {
            setCollapse(false)
            document.body.classList.remove('sidebar-icon-only')
        }
    }

    const handleProfile = () => {
        const profile = document.getElementById('nav-profile')
        const navDown = document.getElementById('profile-dropdown')
        if (isProfileShop == false) {
            setProfShow(true)
            profile.classList.add('show')
            navDown.classList.add('show')
        } else {
            setProfShow(false)
            profile.classList.remove('show')
            navDown.classList.remove('show')
        }
    }

    const handleNotification = () => {
        const notification = document.getElementById('notification')
        if(isNotif == false){
            setNotif(true)
            notification.classList.add('show')
        }else{
            setNotif(false)
            notification.classList.remove('show')
        }
    }

    const logout = () =>{
        localStorage.removeItem('token')
        Router.push('/login')
    }



    return (
        <nav className="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div className="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                <a className="navbar-brand brand-logo" href="index.html"><img style={{height:48,width:'auto'}}  src="assets/images/climeet-2-removedbg.png" alt="logo" /></a>
                <a className="navbar-brand brand-logo-mini"href="index.html"><img  style={{height:58,width:'auto'}}  src="assets/images/climeet-1-removedbg.png" alt="logo" /></a>
            </div>

            <div className="navbar-menu-wrapper d-flex align-items-stretch">
                {/* <button className="navbar-toggler navbar-toggler align-self-center" type="button"> */}
                {/* className={`${isCollapse ? 'navbar-toggler navbar-toggler align-self-center sidebar-icon-only' : 'navbar-toggler navbar-toggler align-self-center'}`} */}
                <button type="button" className="navbar-toggler navbar-toggler align-self-center" onClick={handleCollapse} >
                    <span className="mdi mdi-menu"></span>
                </button>

                {/* <div className="search-field d-none d-md-block">
                    <form className="d-flex align-items-center h-100" action="#">
                        <div className="input-group">
                            <div className="input-group-prepend bg-transparent">
                                <i className="input-group-text border-0 mdi mdi-magnify"></i>
                            </div>
                            <input type="text" className="form-control bg-transparent border-0" placeholder="Search projects" />
                        </div>
                    </form>
                </div> */}

                <ul className="navbar-nav navbar-nav-right">
                    {/* <li className="nav-item nav-profile dropdown" id="nav-profile">
                        <a type="button" onClick={handleProfile} className="nav-link" id="profileDropdown" href="#" >
                            <div className="nav-profile-img">
                                <img src="assets/images/faces/face1.jpg" alt="image" />
                                <span className="availability-status online"></span>
                            </div>
                            <div className="nav-profile-text">
                                <p className="mb-1 text-black">Vergie Ericson</p>
                            </div>
                        </a>
                    </li> */}
                    
                    
                    {/* <li onClick={ handleNotification } className="nav-item dropdown" >
                        <a className="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                            <i className="mdi mdi-bell-outline"></i>
                            <span className="count-symbol bg-danger"></span>
                        </a>
                        <div className="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" id="notification">
                            <h6 className="p-3 mb-0">Notifications</h6>
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item preview-item">
                                <div className="preview-thumbnail">
                                    <div className="preview-icon bg-success">
                                        <i className="mdi mdi-calendar"></i>
                                    </div>
                                </div>
                                <div className="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                    <h6 className="preview-subject font-weight-normal mb-1">Event today</h6>
                                    <p className="text-gray ellipsis mb-0"> Just a reminder that you have an event today </p>
                                </div>
                            </a>
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item preview-item">
                                <div className="preview-thumbnail">
                                    <div className="preview-icon bg-warning">
                                        <i className="mdi mdi-settings"></i>
                                    </div>
                                </div>
                                <div className="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                    <h6 className="preview-subject font-weight-normal mb-1">Settings</h6>
                                    <p className="text-gray ellipsis mb-0"> Update dashboard </p>
                                </div>
                            </a>
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item preview-item">
                                <div className="preview-thumbnail">
                                    <div className="preview-icon bg-info">
                                        <i className="mdi mdi-link-variant"></i>
                                    </div>
                                </div>
                                <div className="preview-item-content d-flex align-items-start flex-column justify-content-center">
                                    <h6 className="preview-subject font-weight-normal mb-1">Launch Admin</h6>
                                    <p className="text-gray ellipsis mb-0"> New admin wow! </p>
                                </div>
                            </a>
                            <div className="dropdown-divider"></div>
                            <h6 className="p-3 mb-0 text-center">See all notifications</h6>
                        </div>
                    </li> */}
                    <li className="nav-item nav-logout d-none d-lg-block">
                        <a onClick={()=>{
                            props.signOut()
                        }} className="nav-link" href="#">
                            <i className="mdi mdi-logout"></i> logout
                        </a>
                    </li>

                </ul>
                <button type="button" onClick={handleCollapseMobile} className="navbar-toggler navbar-toggler-right d-lg-none align-self-center" >
                    <span className="mdi mdi-menu"></span>
                </button>
            </div>
        </nav>
    )
}

export default Navbar