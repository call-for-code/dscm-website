import React from 'react'
import Link from 'next/link'


class Sidebar extends React.Component {

    state = {
        username: ""
    }

    componentDidMount(){
        this.setState({username:localStorage.getItem("username")})
    }

    render() {
        return (
            <nav className="sidebar sidebar-offcanvas" id="sidebar">
                <ul className="nav">
                    <li className="nav-item nav-profile">
                        <a href="#" className="nav-link">
                            <div className="nav-profile-image">
                                <img src="assets/images/faces/face1.jpg" alt="profile" />
                                <span className="login-status online"></span>

                            </div>
                            <div className="nav-profile-text d-flex flex-column">
                                <span className="font-weight-bold mb-2">Howdy, {this.state.username}</span>
                            </div>
                        </a>
                    </li>
                    <li className="nav-item">
                        <Link href="home">
                            <a className="nav-link">
                                <span className="menu-title">Dashboard</span>
                                <i className="mdi mdi-home menu-icon"></i>
                            </a>
                        </Link>
                    </li>


                    <li className="nav-item">
                        <Link href="mapview">
                            <a className="nav-link">
                                <span className="menu-title">Map View</span>
                                <i className="mdi mdi-crosshairs-gps menu-icon"></i>
                            </a>
                        </Link>
                    </li>

                    <li className="nav-item">
                        <Link href="timelineanalyzer">
                            <a className="nav-link">
                                <span className="menu-title">Timeline Analyzer</span>
                                <i className="mdi mdi-chart-timeline menu-icon"></i>
                            </a>
                        </Link>
                    </li>
{/* 
                    <li className="nav-item">
                        <a className="nav-link" href="#">
                            <span className="menu-title">User Management</span>
                            <i className="mdi mdi-contacts menu-icon"></i>
                        </a>
                    </li> */}


                </ul>
            </nav>
        )
    }
}

export default Sidebar