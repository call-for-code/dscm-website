import axios from 'axios'
export default class BaseHelper {

    static BASE_URL = "https://climate-backend.herokuapp.com/"
    url = BaseHelper.BASE_URL

    constructor(url = null) {
        this.url = url != null ? url : BaseHelper.BASE_URL;
    }

    static createFromUrl(url) {
        return new BaseHelper(url);
    }

    static createFromUrlWithBaseUrl(url) {
        return new BaseHelper(BaseHelper.BASE_URL + url);
    }

    static createFromUrlWithApiUrl(url) {
        return new BaseHelper(BaseHelper.BASE_URL + "api/" + url);
    }

    get = (callback) => {
        return axios({
            method: 'GET',
            url: this.url,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + localStorage.getItem('token')
            },
        })
            .then(result => { callback(result.data) })
            .catch(error => {
                console.error(error);
            })
    }


    insert = (data, callback) => {
        return axios({
            method: 'PUT',
            data: data,
            url: this.url,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + localStorage.getItem('token')
            },
        })
            .then(result => { callback(result.data) })
            .catch(error => {
                console.error(error);
            })
    }
}