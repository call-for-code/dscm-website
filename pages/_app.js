// Next Library
import Head from 'next/head'
import Router from 'next/router'
import App from 'next/app'
// Stylesheet
import '../stylesheets/style.scss'
import '../stylesheets/login.scss'
import '../stylesheets/materialdesignicons.min.scss'
import '../stylesheets/dashboard.scss'
// Context
import { UserContext } from './helpers/authContext'

// Layout Default
import DefaultLayout from '../components/layouts/DefaultLayout'

// Fetch Library
import fetch from 'isomorphic-unfetch'

// Loading lib
import NProgress from 'nprogress'

Router.events.on('routeChangeStart', url => {
    console.log(`Loading: ${url}`)
    NProgress.start()
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

class ClimeetApp extends App {


    state = {
        isLogin: false
    }




    signIn = async (email, password) => {

        const url = 'https://climate-backend.herokuapp.com/user/login'
        try {
            const res = await fetch(url, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    username: email,
                    password: password
                })
            })
            if (res.ok) {
                const result = await res.json()                // const result = "Dummy :"

                if (result.status == false) {
                    alert(result.message)
                } else {
                    localStorage.setItem('token', result.data)
                    localStorage.setItem('username', email)
                    this.setState({
                        isLogin: true,
                        token: localStorage.getItem('token')
                    })
                    Router.push('/home')
                    // setLogin(true)
                }
            } else {
                let error = new Error(res.statusText)
                error.res = res
                return Promise.reject(error)
            }

        } catch (error) {
            console.error(
                'You have an error in your code or there are Network issues.',
                error
            )
            throw new Error(error)
        }
    }

    signOut = () => {
        localStorage.removeItem('token');

        this.setState({
            isLogin: false,
        })
        Router.push('/login')
    }


    render() {
        const { Component, pageProps } = this.props
        const Layout = Component.Layout || DefaultLayout

        return (
            <div>
                <div>
                    <Head>
                        <title>Climeet</title>
                        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossOrigin="anonymous"></link>
                        <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.css' />
                        <script src='https://unpkg.com/nprogress@0.2.0/nprogress.js'></script>
                        <link rel='stylesheet' href='https://unpkg.com/nprogress@0.2.0/nprogress.css' />
                    </Head>
                </div>

                <Layout signOut={this.signOut}>
                    <UserContext.Provider value={{ token: this.state.token, signIn: this.signIn, signOut: this.signOut }}>
                        <Component {...pageProps} signOut={this.signOut} />
                    </UserContext.Provider>
                </Layout>
            </div>
        )
    }
}

export default ClimeetApp