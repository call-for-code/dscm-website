
import DashLayout from '../components/layouts/DashboardLayout'

import { useEffect, useState } from 'react'
import Router from 'next/router'

import fetch from 'isomorphic-unfetch'

const Home = () => {

    const [timeline, setTimeline] = useState({
        userLength: null,
        timelineLength: null,
        geoLength: null,
    })

    async function fetchUser(token) {
        const url = 'https://climate-backend.herokuapp.com/api/users'
        const res = await fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            },
        })

        const result = await res.json()

        setTimeline({
            userLength: result.data.length
        })
    }


    useEffect(() => {
        const token = localStorage.getItem('token')
        if (!token) {
            Router.push('/login')
        }

        async function fetchData(token) {
            const url1 = 'https://climate-backend.herokuapp.com/location/user'
            const url2 = 'https://climate-backend.herokuapp.com/api/user_timelines'
            const url3 = 'https://climate-backend.herokuapp.com/api/users'
            const res1 = await fetch(url1, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`,
                },
            })
            const res2 = await fetch(url2, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`,
                },
            })
            const res3 = await fetch(url3, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`,
                },
            })

            const geoJson = await res1.json()
            const timelineJson = await res2.json()
            const usersJson = await res3.json()
    
            
            setTimeline({
                geoLength : geoJson.data.length,
                timelineLength : timelineJson.data.length,
                userLength : usersJson.data.length
            })
    
        
        }

        fetchData(token)
    

       
    }, [])

    return (
        <div className="content-wrapper">

            <div className="page-header">
                <h3 className="page-title">
                    <span className="page-title-icon bg-gradient-primary text-white mr-2">
                        <i className="mdi mdi-home"></i>
                    </span> Dashboard </h3>
                <nav aria-label="breadcrumb">
                    <ul className="breadcrumb">
                        <li className="breadcrumb-item active" aria-current="page">
                            <span></span>Overview <i className="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                        </li>
                    </ul>
                </nav>
            </div>

            <div className="row">

                <div className="col-md-4 stretch-card grid-margin">
                    <div className="card bg-gradient-danger card-img-holder text-white">
                        <div className="card-body">
                            <img src="assets/images/dashboard/circle.svg" className="card-img-absolute" alt="circle-image" />
                            <h4 className="font-weight-normal mb-3">User Registered <i className="mdi mdi-account-group-outline mdi-24px float-right"></i>
                            </h4>
                            <h2 className="mb-5">{timeline.userLength}</h2>
                        </div>
                    </div>
                </div>

                <div className="col-md-4 stretch-card grid-margin">
                    <div className="card bg-gradient-info card-img-holder text-white">
                        <div className="card-body">
                            <img src="assets/images/dashboard/circle.svg" className="card-img-absolute" alt="circle-image" />
                            <h4 className="font-weight-normal mb-3">Timeline Posted <i className="mdi mdi-chart-timeline mdi-24px float-right"></i>
                            </h4>
                            <h2 className="mb-5"> {timeline.timelineLength}  </h2>

                        </div>
                    </div>
                </div>

                <div className="col-md-4 stretch-card grid-margin">
                    <div className="card bg-gradient-success card-img-holder text-white">
                        <div className="card-body">
                            <img src="assets/images/dashboard/circle.svg" className="card-img-absolute" alt="circle-image" />
                            <h4 className="font-weight-normal mb-3">Geo Data Updated <i className="mdi mdi-earth mdi-24px float-right"></i>
                            </h4>
                            <h2 className="mb-5"> {timeline.geoLength} </h2>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    )
}


Home.Layout = DashLayout

export default Home