import Link from 'next/link'
import { useForm } from "react-hook-form";
import React, { useState, useEffect, useContext } from "react";
import Router from 'next/router'
// Context
import { UserContext } from './helpers/authContext'

const Login = () => {
    // FORM VALIDASI 
    const { register, handleSubmit, errors } = useForm({
        nativeValidation: true
    });

    const { signIn } = useContext(UserContext)


    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    useEffect(() => {
        const token = localStorage.getItem('token')
        if(token){
            Router.push('/home')
        }        
    })

    const onSubmit = () => {
        signIn(email, password)
    }


    return (
        <div>
            <main className="d-flex align-items-center min-vh-100 py-3 py-md-0">
                <div className="container">
                    <div className="card login-card">
                        <div className="row no-gutters">

                            <div className="col-md-5">
                                <img src="/static/login.jpg" alt="login" className="login-card-img" />
                            </div>

                            <div className="col-md-7">

                                <div className="card-body">

                                    <div className="brand-wrapper">
                                        <img src="/assets/images/climeet-2-removedbg.png" style={{height:68}} alt="logo" className="logo" />
                                    </div>

                                    <p className="login-card-description">Sign into your account</p>

                                    <form onSubmit={(e) => e.preventDefault()}>

                                        <div className="form-group">
                                            <label htmlFor="email" className="sr-only">Email</label>
                                            <input
                                                type="email"
                                                name="email"
                                                id="email"
                                                className="form-control"
                                                placeholder="Email address"
                                                ref={register({
                                                    required: true
                                                })}
                                                value={email}
                                                onChange={(e) => setEmail(e.target.value)}

                                            />
                                            {errors.email && <span style={{ fontSize: 12, color: "RED" }}>This field is required</span>}
                                        </div>

                                        <div className="form-group mb-4">
                                            <label htmlFor="password" className="sr-only">Password</label>
                                            <input
                                                type="password"
                                                name="password"
                                                id="password"
                                                className="form-control"
                                                placeholder="***********"
                                                ref={register({
                                                    required: true
                                                })}
                                                value={password}
                                                onChange={(e) => setPassword(e.target.value)}

                                            />
                                            {errors.password && <span style={{ fontSize: 12, color: "RED" }}>This field is required</span>}
                                        </div>

                                        <input onClick={handleSubmit(onSubmit)} name="login" id="login" className="btn btn-block login-btn mb-4" type="button" value="Login" />

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </main>

        </div>
    )
}

export default Login