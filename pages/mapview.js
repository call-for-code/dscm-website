import dynamic from 'next/dynamic';
import DashLayout from '../components/layouts/DashboardLayout'
import {useEffect} from 'react'

const MapNoSSR = dynamic(() => import('../components/Map'), {
    loading: () => <p>Loading....</p>,
    ssr: false
});

const MapView = () => {

    useEffect(() => {
        const token = localStorage.getItem('token')

        if(!token){
            Router.push('/login')
        }
    })

    return (
        <div>
            <MapNoSSR />
        </div>
    )
}


MapView.Layout = DashLayout

export default MapView
