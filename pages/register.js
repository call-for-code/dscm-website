import Link from 'next/link'
import { useForm } from "react-hook-form";
import React, { useRef } from "react";

const Register = () => {

    const { register, handleSubmit, watch, errors } = useForm({
        nativeValidation: true
    });
    const password = useRef({});
    password.current = watch("password", "");
    
    const onSubmit = async data => {
        //TODO LOGIC HERE
    };

    return (
        <div>
            <main className="d-flex align-items-center min-vh-100 py-3 py-md-0">
                <div className="container">
                    <div className="card login-card">
                        <div className="row no-gutters">
                            <div className="col-md-5">
                                <img src="/static/login.jpg" alt="login" className="login-card-img" />
                            </div>
                            <div className="col-md-7">
                                <div className="card-body">
                                    <div className="brand-wrapper">
                                        <img src="/static/logo.svg" alt="logo" className="logo" />
                                    </div>
                                    <p className="login-card-description">Register Your Account</p>

                                    <form onSubmit={e => e.preventDefault()} >

                                        <div className="form-group">
                                            <label for="fullname" className="sr-only">Full Name</label>
                                            <input type="text"
                                                name="fullname"
                                                id="fullname"
                                                className="form-control"
                                                placeholder="Your Fullname"
                                                ref={register({ 
                                                    required: true, 
                                                    maxLength : 20
                                                })}
                                            />
                                            {errors.fullname && <span style={{ fontSize: 12, color: "RED" }}>This field is required</span>}
                                        </div>

                                        <div className="form-group">
                                            <label for="email" className="sr-only">Email</label>
                                            <input type="email"
                                                name="email"
                                                id="email" 
                                                className="form-control" 
                                                placeholder="Email address" 
                                                ref={register({
                                                    required: true,
                                                    pattern: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
                                                    
                                                })}
                                            />
                                            {errors.email && <span style={{ fontSize: 12, color: "RED" }}>Please input email correctly </span>}
                                        </div>

                                        <div className="form-group mb-4">
                                            <label for="password" className="sr-only">Password</label>
                                            <input
                                                type="password"
                                                ref={register({
                                                    required: "You must specify a password",
                                                    minLength: {
                                                        value: 8,                                                        
                                                    }
                                                })}
                                                name="password"
                                                id="password"
                                                className="form-control"
                                                placeholder="Password"
                                            />
                                            {errors.password && <span style={{ fontSize: 12, color: "RED" }}>This field is required</span>}
                                        </div>

                                        <div className="form-group mb-4">
                                            <label for="c_password" className="sr-only">Password</label>
                                            <input
                                                type="password"
                                                ref={register({
                                                    validate: value =>
                                                        value === password.current
                                                })}
                                                name="c_password"
                                                id="c_password"
                                                className="form-control"
                                                placeholder="Confirm Password"
                                            />
                                            {errors.c_password && <span style={{ fontSize: 12, color: "RED" }}>The passwords do not match</span>}

                                        </div>
                                        
                                        <input name="register" onClick={handleSubmit(onSubmit)} id="register" className="btn btn-block login-btn mb-4" type="button" value="Register" />
                                    </form>
                                    <a href="#!" className="forgot-password-link">Forgot password?</a>
                                    <p className="login-card-footer-text">Already have an account? <Link href="login" className="text-reset">Login</Link></p>
                                    <nav className="login-card-footer-nav">
                                        <a href="#!">Terms of use.</a>
                                        <a href="#!">Privacy policy</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </main>

        </div>
    )
}

export default Register