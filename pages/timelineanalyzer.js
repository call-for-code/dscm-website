
import React from 'react'
import DashLayout from '../components/layouts/DashboardLayout'
import TimelineGroupCard from '../components/cards/TimelineGroupCard'
import TimelineCard from '../components/cards/TimelineCard'
import BaseHelper from '../helpers/BaseHelper'



class VIEW_STATE {
    static TIMELINE_CATEGORY = 'TIMELINE_CATEGORY';
    static TIMELINES = 'TIMELINES';
    static CREATE_TIMELINE_CATEGORY = 'CREATE_TIMELINE_CATEGORY';
}

class TimelineAnalyzer extends React.Component {

    timelineCategoryHelper = BaseHelper.createFromUrlWithApiUrl("timeline_categories");
    extractedTimelineHelper = BaseHelper.createFromUrlWithApiUrl("extracted_user_timelines");

    state = {
        timelineCategories: [],
        timelines: [],
        createTimelineCategoryForm: {
            name: "",
            categories: ""
        },
        viewState: VIEW_STATE.TIMELINE_CATEGORY
    }

    refreshTimeline() {
        this.timelineCategoryHelper.get((result) => {
            if (result.status)
                this.setState({ timelineCategories: result.data })
        })
    }

    componentDidMount() {
        this.refreshTimeline();
        var intervalId = setInterval(this.refreshTimeline(), 10000);
        this.setState({ intervalId: intervalId });
    }

    componentWillUnmount() {
        clearInterval(this.state.intervalId);
    }

    getRoute() {
        switch (this.state.viewState) {
            case VIEW_STATE.TIMELINE_CATEGORY:
                return <div className="content-wrapper">
                    <div className="row" style={{ marginBottom: 20 }}>
                        <div className={"col-lg-12"} style={{ textAlign: 'right' }}>
                            <button onClick={() => this.getAllTimelines()} type="button" style={{ paddingLeft: 0, paddingRight: 15,marginRight:10 }} className="btn btn-gradient-dark btn-icon-text">
                                <i className="mdi mdi-eye btn-icon-append" style={{ marginRight: 10 }}></i> Show all timeline
                            </button>
                            <button onClick={() => this.setState({ viewState: VIEW_STATE.CREATE_TIMELINE_CATEGORY })} type="button" style={{ paddingLeft: 0, paddingRight: 15 }} className="btn btn-gradient-success btn-icon-text">
                                <i className="mdi mdi-plus btn-icon-append" style={{ marginRight: 10 }}></i> Add new category
                            </button>
                        </div>
                    </div>
                    <div className="row">
                        {this.state.timelineCategories.map(tc =>
                            <TimelineGroupCard
                                key={tc.id}
                                name={tc.name}
                                count={null != tc.timelines ? tc.timelines.length : 0}
                                onClick={() => {
                                    this.setState({
                                        viewState: VIEW_STATE.TIMELINES,
                                        timelines: typeof tc.timelines != "undefined" ? tc.timelines : []
                                    });
                                }}
                            />)}
                    </div>
                </div >
            case VIEW_STATE.TIMELINES:
                return <div className="content-wrapper">
                    <div className="row" style={{ marginBottom: 20 }}>
                        <div className={"col-lg-12"}>
                            <button onClick={() => this.setState({ viewState: VIEW_STATE.TIMELINE_CATEGORY })} type="button" style={{ paddingLeft: 0, paddingRight: 15 }} className="btn btn-gradient-dark btn-icon-text">
                                <i className="mdi mdi-arrow-left btn-icon-append" style={{ marginRight: 10 }}></i> Back
                        </button>
                        </div>
                    </div>
                    <div className="row">
                        {this.state.timelines.map(t => <TimelineCard
                            description={t.description}
                            category={t.category_relevant_text}
                            labels={t.category_label.split("/")}
                            username={t.username}
                            createdAt={t.created_at + " UTC"}
                            mediaType={typeof t.media_type != 'undefined' ? t.media_type : null}
                            mediaUrl={typeof t.media_url != 'undefined' ? t.media_url : null}
                        />)}
                    </div>
                </div >
            case VIEW_STATE.CREATE_TIMELINE_CATEGORY:
                return <div className="content-wrapper">
                    <div className="row" style={{ marginBottom: 20 }}>
                        <div className={"col-lg-12"}>
                            <button onClick={() => this.setState({ viewState: VIEW_STATE.TIMELINE_CATEGORY })} type="button" style={{ paddingLeft: 0, paddingRight: 15 }} className="btn btn-gradient-dark btn-icon-text">
                                <i className="mdi mdi-arrow-left btn-icon-append" style={{ marginRight: 10 }}></i> Back
                        </button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 grid-margin stretch-card" >
                            <div className="card">
                                <div className="card-body">
                                    <h4 className="card-title">Add new category</h4>
                                    <form className="forms-sample">
                                        <div className="form-group">
                                            <label htmlFor="exampleInputUsername1">Name</label>
                                            <input type="text" onChange={(e) => (this.setState({
                                                createTimelineCategoryForm: {
                                                    name: e.target.value,
                                                    categories: this.state.createTimelineCategoryForm.categories,
                                                }
                                            }))} className="form-control" id="exampleInputUsername1" placeholder="Example : Volcanic Eruption" style={{ backgroundImage: 'url("data:image/png', backgroundRepeat: 'no-repeat', backgroundAttachment: 'scroll', backgroundSize: '16px 18px', backgroundPosition: '98% 50%' }} />
                                        </div>
                                        <div className="form-group">
                                            <label for="exampleTextarea1">Categories (separate using comma ,)</label>
                                            <textarea className="form-control" onChange={(e) => (this.setState({
                                                createTimelineCategoryForm: {
                                                    name: this.state.createTimelineCategoryForm.name,
                                                    categories: e.target.value
                                                }
                                            }))} id="exampleTextarea1" placeholder="Example : eruption,volcanic,mountain" rows="4"></textarea>
                                        </div>
                                        <button type="button" onClick={() => { this.createNewCategory() }} className="btn btn-gradient-primary mr-2">Submit</button>
                                        <button onClick={() => this.clearAddForm()} className="btn btn-light">Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div >
            default:
                return null;
        }
    }


    getAllTimelines = () => {
        this.extractedTimelineHelper.get((res)=>{
            if(res.status){
                this.setState({
                    viewState: VIEW_STATE.TIMELINES,
                    timelines: res.data
                });
            }
        })
    }

    createNewCategory = () => {
        if (!(null != this.state.createTimelineCategoryForm.name && "" != this.state.createTimelineCategoryForm.name.trim())) {
            alert('Name must not empty');
            return;
        } else if (!(null != this.state.createTimelineCategoryForm.categories && "" != this.state.createTimelineCategoryForm.categories.trim())) {
            alert('Categories must not empty');
            return;
        }

        this.timelineCategoryHelper.insert(this.state.createTimelineCategoryForm, (data) => {
            if (data.status) {
                this.clearAddForm()
                this.refreshTimeline()
            } else {
                alert('Failed to insert: ' + data.message)
            }
        })
    }

    clearAddForm = () => {
        this.setState({
            viewState: VIEW_STATE.TIMELINE_CATEGORY, createTimelineCategoryForm: {
                categories: "",
                name: ""
            }
        })
    }

    render() {
        return this.getRoute();

    }
}

TimelineAnalyzer.Layout = DashLayout

export default TimelineAnalyzer

